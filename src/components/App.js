import Courses from "./Courses";
import Dishes from "./Dishes";

export const App = () => {
  return (
    <>
      <Courses />
      <Dishes />
    </>
  );
};
