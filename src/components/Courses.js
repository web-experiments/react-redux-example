import { selectCourse } from "../actions";

const Courses = ({ course, onSelectCourse }) => {
  const onChange = (event) => {
    onSelectCourse(event.target.value);
  };
  return (
    <>
      <label forlabel="courses">Choose a course:</label>
      <div>
        <select value={course} onChange={onChange} name="courses" id="courses">
          <option value="starter">Starter</option>
          <option value="main">Main</option>
          <option value="dessert">Dessert</option>
        </select>
      </div>
    </>
  );
};
const mapDispatchToProps = (dispatch) => {
  return {
    onSelectCourse: (course) => dispatch(selectCourse(course)),
  };
};
const mapStateToProps = (state) => {
  return {
    course: state.course,
  };
};
export default ReactRedux.connect(mapStateToProps, mapDispatchToProps)(Courses);
