const STARTERS = [
  "Choice Duck & Liver Parfait with Red Onion Jam",
  "Prawn & Avocado Cocktail",
  "Smoked Salmon served with horseradish crème Fraiche & Mixed Leaves",
  "Creamy Garlic Mushrooms on Ciabatta",
  "Smoked Salmon Crayfish & Dill Mousse",
  "Goats Cheese & Onion Filo Tart",
];
const MAINS = [
  "Fillet steak served with a mushroom sauce served with Dauphinoise Potatoes",
  "Pan fried Sea Bass with crispy pancetta Served on a bed of sweet potato",
  "Vegetable Nut Roast with Apricot & Goats Cheese",
  "Pumpkin and Red Onion Tagine",
];
const DESSERTS = [
  "Individual Chocolate & Lime Cheese cake",
  "Crème Brulee",
  "Rhubarb & Apple Crumble",
];

const Dishes = ({ course }) => {
  const dishes = {
    starter: STARTERS.map((dish, index) => ({ value: index, text: dish })),
    main: MAINS.map((dish, index) => ({ value: index, text: dish })),
    dessert: DESSERTS.map((dish, index) => ({ value: index, text: dish })),
  };

  const [dish, setDish] = React.useState();

  React.useEffect(() => {
    setDish(dishes[course][0].value.toString());
  }, [course]);

  const onChange = (event) => {
    setDish(event.target.value);
  };
  return (
    <>
      <label forlabel="dishes">Choose a dish:</label>
      <div>
        <select value={dish} onChange={onChange} name="dishes" id="dishes">
          {dishes[course].map((dish) => {
            return (
              <option key={dish.value} value={dish.value}>
                {dish.text}
              </option>
            );
          })}
        </select>
      </div>
    </>
  );
};
const mapStateToProps = (state) => {
  return {
    course: state.course,
  };
};
export default ReactRedux.connect(mapStateToProps)(Dishes);
