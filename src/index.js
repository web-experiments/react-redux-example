import { App } from "./components/App";

const initialState = { course: "starter" };
const appReducer = (state = initialState, action) => {
  if (action.type === "courseSelected") {
    return {
      ...state,
      course: action.payload,
    };
  }
  return state;
};
const store = Redux.createStore(appReducer);
ReactDOM.render(
  <ReactRedux.Provider store={store}>
    <App />
  </ReactRedux.Provider>,
  document.getElementById("root")
);
